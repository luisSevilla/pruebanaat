package com.example.pruebanaat.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.pruebanaat.R
import com.example.pruebanaat.companies.CompaniesActivity
import com.example.pruebanaat.login.model.LoginResponse
import com.example.pruebanaat.login.service.ServiceLogin
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.util.*


class LoginActivity: AppCompatActivity() {
    var call: Call<LoginResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            if (edt_user.text.isEmpty() || edt_pass.text.isNullOrEmpty()){
                Toast.makeText(this@LoginActivity,"Ingrese Usuario y/o Contraseña",Toast.LENGTH_SHORT).show()
            }else {
                progressBarlayout.visibility = View.VISIBLE
                getlogin(edt_user.text.toString(), edt_pass.text.toString())
            }
        }
    }

    private fun getlogin(user: String, pass: String){
        val pasEncrip = hashWith256(pass)
        call = getRetrofit().create(ServiceLogin::class.java).getUser("password",user.trim(),"a0700af71a183b82aa4d79682475b151161bf91138d77f6f10937240f40814bd")
        call?.enqueue(object : Callback<LoginResponse?> {
            override fun onResponse(call: Call<LoginResponse?>, response: Response<LoginResponse?>) {
                val loginResponse = response.body()

                if (loginResponse?.accessToken != null) {
                    progressBarlayout.visibility = View.GONE
                    val mIntent = Intent(this@LoginActivity, CompaniesActivity::class.java)
                    startActivity(mIntent)
                    finish()
                } else {
                    progressBarlayout.visibility = View.GONE
                    Toast.makeText(this@LoginActivity,"Fail lOGIN",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                progressBarlayout.visibility = View.GONE
                Toast.makeText(this@LoginActivity,"Fail lOGIN",Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://uat.firmaautografa.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


    @SuppressLint("NewApi")
    fun hashWith256(textToHash: String): String? {
        val digest = MessageDigest.getInstance("SHA-256")
        val byteOfTextToHash = textToHash.toByteArray(StandardCharsets.UTF_8)
        val hashedByetArray = digest.digest(byteOfTextToHash)
        return Base64.getEncoder().encodeToString(hashedByetArray)
    }


}