package com.example.pruebanaat.login.model

import com.google.gson.annotations.SerializedName

data class LoginResponse (
    @SerializedName("access_token")
    var accessToken: String? = null,

    @SerializedName("token_type")
    var tokenType: String? = null,

    @SerializedName("refresh_token")
    var refreshToken: String? = null,

    @SerializedName("expires_in")
var expiresIn: String? = null,

@SerializedName("scope")
var scope: String? = null,

@SerializedName("jti")
var jti: String? = null


    )