package com.example.pruebanaat.login.model

import com.google.gson.annotations.SerializedName




data class UserModel (
    @SerializedName("grant_type")
    var grantType: String? = null,

    @SerializedName("username")
    var username: String? = null,

    @SerializedName("password")
    var password: String? = null
)