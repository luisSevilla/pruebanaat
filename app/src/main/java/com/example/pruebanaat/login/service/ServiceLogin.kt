package com.example.pruebanaat.login.service

import com.example.pruebanaat.login.model.LoginResponse
import retrofit2.Call
import retrofit2.http.*


interface ServiceLogin{

    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded",
    "Authorization: Basic Wm1Ga0xXTXlZeTF3YjNKMFlXdz06TWpoa04yUTNNbUppWVRWbVpHTTBObVl4Wmpka1lXSmpZbVEyTmpBMVpEVXpaVFZoT1dNMVpHVTROakF4TldVeE9EWmtaV0ZpTnpNd1lUUm1ZelV5WWc9PQ==")
    @POST("authorization-server/oauth/token")
    fun getUser(@Field("grant_type") grant_type: String?, @Field("username") username: String?, @Field("password") password: String?): Call<LoginResponse>
}