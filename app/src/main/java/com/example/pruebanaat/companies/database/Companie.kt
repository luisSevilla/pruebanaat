package com.example.pruebanaat.companies.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Companie (
    @PrimaryKey val uid: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "opc") val opc: String?,
    @ColumnInfo(name = "opc2") val opc2: String?,
    @ColumnInfo(name = "opc3") val opc3: String?
    )