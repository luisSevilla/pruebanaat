package com.example.pruebanaat.companies.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.pruebanaat.R
import com.example.pruebanaat.companies.database.Companie
import com.example.pruebanaat.recharge.ContainerActivity
import java.util.*
import kotlin.collections.ArrayList

class CompanieAdapter(var context: FragmentActivity, var listCompanie: ArrayList<Companie>): RecyclerView.Adapter<CompanieAdapter.ViewHolder>(),
    Filterable {
    val TAG = "Companieaadapter"
    var companieFilterList = ArrayList<Companie>()

    init {
        companieFilterList = listCompanie
    }
    inner class  ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_name_companie : TextView = itemView.findViewById(R.id.sectionTitleTextView)
        var tv_img : ImageView = itemView.findViewById(R.id.imageView)
        var tv_img1 : ImageView = itemView.findViewById(R.id.imageView1)
        var tv_img2: ImageView = itemView.findViewById(R.id.imageView2)
        var tv_name_img : TextView = itemView.findViewById(R.id.titleTextView)
        var tv_name_img1 : TextView = itemView.findViewById(R.id.titleTextView1)
        var tv_name_img2 : TextView = itemView.findViewById(R.id.titleTextView2)
        var ln_opc: LinearLayout = itemView.findViewById(R.id.ln_opc1)
        var ln_opc2: LinearLayout = itemView.findViewById(R.id.ln_opc2)
        var ln_opc3: LinearLayout = itemView.findViewById(R.id.ln_opc3)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_companie_section, parent,false))
    }

    override fun getItemCount(): Int {
        return companieFilterList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        if (listCompanie[i].name != "" )
        holder.tv_name_companie.text = listCompanie[i].name

        if (listCompanie[i].opc != "" ) {
            holder.tv_name_img.text = listCompanie[i].opc
            holder.ln_opc.visibility = View.VISIBLE
        }

        if (listCompanie[i].opc2 != "" ) {
            holder.tv_name_img1.text = listCompanie[i].opc2
            holder.ln_opc2.visibility = View.VISIBLE
        }

        if (listCompanie[i].opc3 != "" ) {
            holder.tv_name_img2.text = listCompanie[i].opc3
            holder.ln_opc3.visibility = View.VISIBLE
        }

        Log.d("nombreff", listCompanie[i].name.toString())
        if (listCompanie[i].name == "Claro"){
            holder.tv_img.setImageResource(R.drawable.ic_claro)
            holder.tv_img1.setImageResource(R.drawable.ic_claro)
            holder.tv_img2.setImageResource(R.drawable.ic_claro)
        }
        if (listCompanie[i].name == "Tuenti"){
            holder.tv_img.setImageResource(R.drawable.ic_tuenti)
            holder.tv_img1.setImageResource(R.drawable.ic_tuenti)
            holder.tv_img2.setImageResource(R.drawable.ic_tuenti)
        }
        if (listCompanie[i].name == "Entel"){
            holder.tv_img.setImageResource(R.drawable.ic_entel)
            holder.tv_img1.setImageResource(R.drawable.ic_entel)
            holder.tv_img2.setImageResource(R.drawable.ic_entel)
        }

        holder.tv_img.setOnClickListener {
            Log.d("cliclimv","0")
            goToRechargeFragment(holder.tv_name_companie.text.toString())
        }

        holder.tv_img1.setOnClickListener {
            Log.d("cliclimv","1")
            goToRechargeFragment(holder.tv_name_companie.text.toString())
        }

        holder.tv_img2.setOnClickListener {
           goToRechargeFragment(holder.tv_name_companie.text.toString())
        }


    }

    private fun goToRechargeFragment(companie:String){
        val mIntent = Intent(context, ContainerActivity::class.java)
        context.startActivity(mIntent)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    companieFilterList = listCompanie
                } else {
                    val resultList = ArrayList<Companie>()
                    for (row in listCompanie) {
                        if (row.name?.lowercase(Locale.ROOT)?.contains(charSearch.lowercase(Locale.ROOT))!!) {
                            resultList.add(row)
                        }
                    }
                    companieFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = companieFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                companieFilterList = results?.values as ArrayList<Companie>
                notifyDataSetChanged()
            }

        }
    }
}