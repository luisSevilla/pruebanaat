package com.example.pruebanaat.companies.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CompanieDAO {
    @Query("SELECT * FROM companie")
    suspend fun getAll(): List<Companie>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(companies: List<Companie>)
}