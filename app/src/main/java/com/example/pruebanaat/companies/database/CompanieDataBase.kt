package com.example.pruebanaat.companies.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Companie::class), version = 1)
abstract class CompanieDataBase: RoomDatabase() {
    abstract fun companieDao(): CompanieDAO
}