package com.example.pruebanaat.companies


import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.SearchView

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.pruebanaat.R
import com.example.pruebanaat.companies.adapter.CompanieAdapter
import com.example.pruebanaat.companies.database.Companie
import com.example.pruebanaat.companies.database.CompanieDataBase
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CompaniesActivity: AppCompatActivity() {
    var listcompanies: ArrayList<Companie> = arrayListOf()
    var allComp:ArrayList<Companie> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val db = Room.databaseBuilder(
            applicationContext,
            CompanieDataBase::class.java, "database-name"
        ).build()
        var c1 = Companie(1,"Claro","recarga","Tiempo","")
        var c2 = Companie(2,"Tuenti","recarga","Megas","Tiempo")
        var c3 = Companie(3,"Entel","TIEMPO","","")

        listcompanies.add(c1)
        listcompanies.add(c2)
        listcompanies.add(c3)
        CoroutineScope(Dispatchers.Main).launch {
            db.companieDao().insert(listcompanies)
        }

        rv_list_companies.setHasFixedSize(false)
        val mLayoutManager = LinearLayoutManager(this)
        rv_list_companies.layoutManager = mLayoutManager
        

        CoroutineScope(Dispatchers.Main).launch {
            var com=db.companieDao().getAll()
            com.forEach {
               allComp.add(it)
            }
            Log.d("sizeComp", allComp.size.toString())
            rv_list_companies.adapter = CompanieAdapter(this@CompaniesActivity, com as ArrayList<Companie>)
        }

        country_search.setOnQueryTextListener(object: SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
               val adap = rv_list_companies.adapter as CompanieAdapter
                adap.filter.filter(newText)
                return false
            }

        })
    }
}