package com.example.pruebanaat.recharge

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.pruebanaat.R
import kotlinx.android.synthetic.main.fragment_recharge.*

class RechargeFragment: Fragment() {

    var compa: String? = null

    companion object{
        val TAG: String = "RechargeFragment"

        fun newInstance(companie: String): RechargeFragment {
            val fragment = RechargeFragment()

            val args = Bundle()
                args.putString("COMPANIE", companie)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            compa = it.getString("COMPANIE")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recharge, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cel = edt_cel.text
        val monto = edt_monto.text

        btn_continue.setOnClickListener {
            if (cel.isNullOrEmpty() || monto.isNullOrEmpty()){
                Toast.makeText(requireContext(),"Ingrese Celular y Monto", Toast.LENGTH_SHORT).show()
            }else {
                val confirmRechargeFragment =
                    ConfirmRechargeFragment.newInstance(compa!!, cel.toString(), monto.toString())
                confirmRechargeFragment.isCancelable = true
                confirmRechargeFragment.show(
                    requireActivity().supportFragmentManager,
                    "DialogConfirm"
                )
            }
        }
    }
}