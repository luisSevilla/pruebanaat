package com.example.pruebanaat.recharge

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.pruebanaat.R
import com.example.pruebanaat.companies.CompaniesActivity
import kotlinx.android.synthetic.main.fragment__succes_recharge.*


class SuccesRechargeFragment: DialogFragment() {

    companion object {
        const val TAG = "SuccesRechargeFragment"
        fun newInstance() = SuccesRechargeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        isCancelable = false
        return inflater.inflate(R.layout.fragment__succes_recharge, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCancelable(false)
        btn_accept_succes.setOnClickListener {
            dialog?.dismiss()
            requireActivity().setResult(Activity.RESULT_OK)
            requireActivity().finish()
        }
    }
}