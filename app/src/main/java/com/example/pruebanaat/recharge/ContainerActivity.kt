package com.example.pruebanaat.recharge

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pruebanaat.R

class ContainerActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_container)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameLayout, RechargeFragment.newInstance("companie"))
            .commit()
    }

}