package com.example.pruebanaat.recharge

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.example.pruebanaat.R
import kotlinx.android.synthetic.main.fragment_confirm_recharge.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ConfirmRechargeFragment : DialogFragment() {

    private var companie: String? = null
    private var celular: String? = null
    private var monto: String? = null

    companion object {
        const val TAG = "PurchaseConfirmationDialog"

        fun newInstance(companie: String, celular: String, monto: String): ConfirmRechargeFragment {
            val args = Bundle()
            args.putString("COMPANIE", companie)
            args.putString("CELULAR", celular)
            args.putString("MONTO", monto)
            val fragment = ConfirmRechargeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            companie = it.getString("COMPANIE")
            celular = it.getString("CELULAR")
            monto = it.getString("MONTO")
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_confirm_recharge, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val df: DateFormat = SimpleDateFormat("dd/MMM/yy", Locale.US)
        val strDate: String = df.format(Date())

        val dfH: DateFormat = SimpleDateFormat("hh:mm:ss", Locale.US)
        val strTime = dfH.format(Date())

        txt_hora.text = strTime
        txt_date.text = strDate
        txt_celular.text = celular
        txt_monto.text = monto

        btn_acept_recharge.setOnClickListener {
            dialog?.dismiss()
                val succesDialog = SuccesRechargeFragment.newInstance()
                succesDialog.isCancelable = false
                succesDialog.show(requireActivity().supportFragmentManager.beginTransaction(), "DialogSucces")
        }
        btn_cancel_recharge.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}